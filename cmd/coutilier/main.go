package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/configor"
	"gitlab.com/MaxPod/coutilier/internal/model"
	"gitlab.com/MaxPod/coutilier/internal/source"
	"gitlab.com/MaxPod/coutilier/internal/store"
	"gitlab.com/MaxPod/coutilier/internal/tools/app"
)

// config - конфигурация приложения
type config struct {
	AlcorobotAddress          string            `json:"alcorobot_address" default:"http://192.168.88.4"`
	PollingInterval           int64             `json:"polling_interval" default:"15"`
	PrometheusExporterAddress string            `json:"prometheus_exporter_address" default:"localhost:8000"`
	AlcoParamsNames           map[string]string `json:"alco_params_names"`
}

func main() {
	// принимаем флаги
	configPath := flag.String("config", "", "config file path")
	flag.Parse()

	// созадем логеры
	l := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime)

	// грузим конфигурацию
	conf := new(config)

	err := configor.Load(conf, *configPath)
	if err != nil {
		errLog.Println("main configor.Load():", err)
		panic(err)
	}

	// alcorobot - источник данных
	alcorobot := source.NewSource(conf.AlcorobotAddress)
	l.Println("Установлен источник данных:", conf.AlcorobotAddress)

	// promik - хранилище данных
	promik := store.NewPrometheusExporter(conf.PrometheusExporterAddress)
	l.Println("Prometheus exporter открыт на :", conf.PrometheusExporterAddress)

	// coutilier - модель данных, основная сущность приложения
	coutilier, err := model.NewCoutilier(conf.PollingInterval, alcorobot, promik, conf.AlcoParamsNames)
	if err != nil {
		errLog.Println("main model.NewCoutilier():", err)
		panic(err)
	}

	// запуск процессов
	appRunners := []app.RunFunc{
		app.SignalNotify,
		coutilier.Run,
		app.RunServerFunc(promik.Server),
	}

	err = app.RunParallel(context.Background(), appRunners...)

	app.Exit(func() error { return fmt.Errorf("app.RunParallel: %w", err) }, l)
}
