#!/bin/bash

export HTTP_CLIENT_RETRY_WAIT_MIN=1s
export HTTP_CLIENT_RETRY_WAIT_MAX=3s
export HTTP_CLIENT_RETRY_MAX=3

go run cmd/coutilier/* -config="./config.json"

