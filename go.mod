module gitlab.com/MaxPod/coutilier

go 1.16

require (
	github.com/hashicorp/go-cleanhttp v0.5.2
	github.com/hashicorp/go-retryablehttp v0.7.0
	github.com/jinzhu/configor v1.2.1
	github.com/prometheus/client_golang v1.11.0
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
	golang.org/x/text v0.3.6
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
