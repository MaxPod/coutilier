package source

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/hashicorp/go-retryablehttp"
	"gitlab.com/MaxPod/coutilier/internal/tools/httputil"
	toolsxml "gitlab.com/MaxPod/coutilier/internal/tools/xml"
)

var err error

// Alcorobot - структура источника, реализует интерфейс Source
type Alcorobot struct {
	dataURL string
	cli     *retryablehttp.Client
}

// Inputs - структура xml-данных от алкоробота
type Inputs struct {
	A1  string `xml:"a1"`
	A2  string `xml:"a2"`
	A3  string `xml:"a3"`
	B1  string `xml:"b1"`
	B2  string `xml:"b2"`
	B3  string `xml:"b3"`
	B4  string `xml:"b4"`
	B5  string `xml:"b5"`
	B8  string `xml:"b8"`
	B9  string `xml:"b9"`
	B10 string `xml:"b10"`
	B11 string `xml:"b11"`
	B12 string `xml:"b12"`
	B13 string `xml:"b13"`
	B14 string `xml:"b14"`
	B15 string `xml:"b15"`
	B16 string `xml:"b16"`
	B17 string `xml:"b17"`
	C1  string `xml:"c1"`
	C2  string `xml:"c2"`
	C3  string `xml:"c3"`
	C4  string `xml:"c4"`
	D1  string `xml:"d1"`
	E1  string `xml:"e1"`
	E2  string `xml:"e2"`
	F1  string `xml:"f1"`
	G1  string `xml:"g1"`
	G7  string `xml:"g7"`
	H1  string `xml:"h1"`
	H2  string `xml:"h2"`
	I1  string `xml:"i1"`
	I2  string `xml:"i2"`
	I3  string `xml:"i3"`
	I4  string `xml:"i4"`
	J1  string `xml:"j1"`
	K1  string `xml:"k1"`
	K2  string `xml:"k2"`
	L2  string `xml:"l2"`
	L4  string `xml:"l4"`
	L6  string `xml:"l6"`
	M2  string `xml:"m2"`
	M3  string `xml:"m3"`
	N1  string `xml:"n1"`
	N2  string `xml:"n2"`
	N3  string `xml:"n3"`
	N4  string `xml:"n4"`
	N5  string `xml:"n5"`
	N6  string `xml:"n6"`
	N7  string `xml:"n7"`
}

// NewSource -  возвращает новый объект Алкоробот
func NewSource(dataURL string) *Alcorobot {
	cli := httputil.NewClient()

	return &Alcorobot{
		dataURL: dataURL,
		cli:     cli,
	}
}

// GetData -  получает мапу данных из источника
func (s *Alcorobot) GetData(ctx context.Context) (map[string]string, error) {
	var m Inputs

	data, err := s.GetDataRequest(ctx)
	if err != nil {
		return nil, fmt.Errorf("GetDataRequest: %w", err)
	}

	err = toolsxml.Decode(*data, &m)
	if err != nil {
		return nil, fmt.Errorf("failed decode response: %w", err)
	}

	r := make(map[string]string)
	r["a1"] = m.A1
	r["a2"] = m.A2
	r["a3"] = m.A3
	r["b1"] = m.B1
	r["b2"] = m.B2
	r["b3"] = m.B3
	r["b4"] = m.B4
	r["b5"] = m.B5
	r["b8"] = m.B8
	r["b9"] = m.B9
	r["b10"] = m.B10
	r["b11"] = m.B11
	r["b12"] = m.B12
	r["b13"] = m.B13
	r["b14"] = m.B14
	r["b15"] = m.B15
	r["b16"] = m.B16
	r["b17"] = m.B17
	r["c1"] = m.C1
	r["c2"] = m.C2
	r["c3"] = m.C3
	r["c4"] = m.C4
	r["d1"] = m.D1
	r["e1"] = m.E1
	r["e2"] = m.E2
	r["f1"] = m.F1
	r["g1"] = m.G1
	r["g7"] = m.G7
	r["h1"] = m.H1
	r["h2"] = m.H2
	r["i1"] = m.I1
	r["i2"] = m.I2
	r["i3"] = m.I3
	r["i4"] = m.I4
	r["j1"] = m.J1
	r["k1"] = m.K1
	r["k2"] = m.K2
	r["l2"] = m.L2
	r["l4"] = m.L4
	r["l6"] = m.L6
	r["m2"] = m.M2
	r["m3"] = m.M3
	r["n1"] = m.N1
	r["n2"] = m.N2
	r["n3"] = m.N3
	r["n4"] = m.N4
	r["n5"] = m.N5
	r["n6"] = m.N6
	r["n7"] = m.N7

	return r, nil
}

// GetDataRequest - выполняет запрос к источнику, возвращает указатель на ответ.
func (c *Alcorobot) GetDataRequest(ctx context.Context) (*[]byte, error) {
	httpReq, err := http.NewRequestWithContext(
		ctx,
		"GET",
		c.dataURL,
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("http.NewRequestWithContext: %w", err)
	}

	retryReq, err := retryablehttp.FromRequest(httpReq)
	if err != nil {
		return nil, fmt.Errorf("retryablehttp.FromRequest: %w", err)
	}

	httpResp, err := c.cli.Do(retryReq)
	if err != nil {
		return nil, fmt.Errorf("failed to do queueSize request: %w", err)
	}

	if httpResp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Bad Status Code: %d", httpResp.StatusCode)
	}

	bodyBytes, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		return nil, fmt.Errorf("error read all response body %w", err)
	}

	return &bodyBytes, httpResp.Body.Close()
}
