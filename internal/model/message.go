package model

import (
	"fmt"
	"strings"
)

// Message - сообщение, в виде матрицы - таблицы значений
type Message struct {
	ID     []string
	Name   []string
	Value  []string
	Length int
}

const (
	MessageID    = "ID"    // идентификатор колонки
	MessageName  = "Name"  // идентификатор колонки
	MessageValue = "Value" // идентификатор колонки
)

// New - создает новую матрицу Message
func NewMessage() *Message {
	m := &Message{}
	m.ID = make([]string, 1)
	m.Name = make([]string, 1)
	m.Value = make([]string, 1)

	return m
}

// Add - добавляет строку в матрицу
func (m *Message) Add(id, name, value string) {
	m.ID = append(m.ID, id)
	m.Name = append(m.Name, name)
	m.Value = append(m.Value, value)
	m.Length++
}

// Find - поиск. Возвращает номер строки по значению поля. field -  имя поля, value - значение поля.
func (m *Message) Find(field string, value interface{}) (int, error) {
	rowNumber := 0

	switch field {
	case MessageID:
		sample, ok := value.(string)
		if !ok {
			return 0, fmt.Errorf("Ошибка преобразования к string - %s", value)
		}

		for i, v := range m.ID {
			if v == sample {
				rowNumber = i

				break
			}
		}

	case MessageName:
		sample, ok := value.(string)
		if !ok {
			return 0, fmt.Errorf("Ошибка преобразования к string - %s", value)
		}

		for i, v := range m.Name {
			if v == sample {
				rowNumber = i

				break
			}
		}

	case MessageValue:
		sample, ok := value.(string)
		if !ok {
			return 0, fmt.Errorf("Ошибка преобразования к string - %s", value)
		}

		for i, v := range m.Value {
			if v == sample {
				rowNumber = i

				break
			}
		}
	default:
		return 0, fmt.Errorf("Не существующее поле Message - %s", field)
	}

	if rowNumber == 0 {
		return 0, fmt.Errorf("Message: в поле %s значение %s не найдено", field, value)
	}

	return rowNumber, nil
}

// Update - обновляет значение value поля field в строке матрицы rowNumber
func (m *Message) Update(rowNumber int, field, value string) error {
	if rowNumber > m.Length {
		err := fmt.Errorf("Номер строки %d больше количества строк %d", rowNumber, m.Length)

		return err
	}

	switch field {
	case MessageID:
		m.ID[rowNumber] = value
	case MessageName:
		m.Name[rowNumber] = value
	case MessageValue:
		m.Value[rowNumber] = value
	default:
		err := fmt.Errorf("Не существующее поле Message - %s", field)

		return err
	}

	return nil
}

// String - Вывод матрицы на экран, метод типа
// nolint:gomnd
func (m *Message) String() string {
	result := " | ID | Value  |   Name                           |\n"
	result += " |----|--------|----------------------------------|\n"

	for i := 1; i <= m.Length; i++ {
		result += " |" + m.ID[i] + getSpace(4, m.ID[i]) +
			"|" + m.Value[i] + getSpace(8, m.Value[i]) +
			"|" + m.Name[i] + getSpace(34, m.Name[i]) + "|\n"
	}

	result += " -------------------------------------------------\n"

	return result
}

// getSpace - рассчитывает пробельные вставки для метода String
func getSpace(full int, val string) string {
	count := full - len(val)
	if count < 0 {
		count = 0
	}

	res := strings.Repeat(" ", count)

	return res
}

// UpdateValueByID - Обновляет значение поля Value с поиском строки по значению ID
func (m *Message) UpdateValueByID(id, value string) error {
	rowNumber, err := m.Find("ID", id)
	if err != nil {
		return fmt.Errorf("m.Find: %w", err)
	}

	err = m.Update(rowNumber, "Value", value)
	if err != nil {
		return fmt.Errorf("m.Update: %w", err)
	}

	return nil
}

// Get - возращает заначения из матрицы по индексу
func (m *Message) Get(i int) (id, name, value string) {
	id = m.ID[i]
	name = m.Name[i]
	value = m.Value[i]

	return id, name, value
}
