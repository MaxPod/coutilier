package model

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

// Source - интерфейс источник данных
type Source interface {
	GetData(ctx context.Context) (map[string]string, error)
}

// Store -  интерфейс хранилища данных
type Store interface {
	SaveParameter(id, name, value string) error
}

// Coutilier -  основная сущность
type Coutilier struct {
	Message   *Message      // сообщение - таблица значений
	source    Source        // источник
	store     Store         // хранилище
	sleeptime time.Duration // интервал опроса источника
	l         *log.Logger   // логгер инфо
	errLog    *log.Logger   // логгер ошибок
}

// NewCoutilier -  конструктор объекта Coutilier
func NewCoutilier(interval int64, source Source, store Store, nameMap map[string]string) (*Coutilier, error) {
	c := &Coutilier{}

	c.l = log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	c.errLog = log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime)

	listParameters := []string{
		"a1", "a2", "a3",
		"b1", "b2", "b3", "b4", "b5", "b8", "b9", "b10", "b11", "b12", "b13", "b14", "b15", "b16", "b17",
		"c1", "c2", "c3", "c4",
		"d1",
		"e1", "e2",
		"f1",
		"g1", "g7",
		"h1", "h2",
		"i1", "i2", "i3", "i4",
		"j1",
		"k1", "k2",
		"l2", "l4", "l6",
		"m2", "m3",
		"n1", "n2", "n3", "n4", "n5", "n6", "n7",
	}

	c.Message = NewMessage()

	for _, id := range listParameters {
		c.Message.Add(id, nameMap[id], "")
	}

	c.sleeptime = time.Duration(interval) * time.Second

	if source == nil {
		return nil, fmt.Errorf("source == nil")
	}

	c.source = source
	c.store = store

	return c, nil
}

const bitSize = 64 // разрадность конвертации строки к числу

const twoDigits = 2 // 2 знака после зпт

const ten = 10 // десять ( для деления )

// GetMessage -  основная бизнес логика:
// получает сообщение из источника
// обрабатывает данные
// передает в стор
func (c *Coutilier) GetMessage(ctx context.Context) {
	// получаем сообщение от робота
	mapData, err := c.source.GetData(ctx)
	if err != nil {
		c.errLog.Println("c.source.GetData():", err)
	}

	// записываем в матрицу
	for id, value := range mapData {
		switch id {
		case "b1", "b2", "b3", "b4", "b5":
			bValue, err := strconv.ParseFloat(value, bitSize)
			if err != nil {
				bValue = 0
			}

			bValue = bValue / ten
			value = strconv.FormatFloat(bValue, 'f', twoDigits, bitSize)

		case "a1", "a2", "a3":
			h, err := strconv.ParseFloat(value[:2], bitSize)
			if err != nil {
				h = 0
			}

			m, err := strconv.ParseFloat(value[3:5], bitSize)
			if err != nil {
				m = 0
			}

			s, err := strconv.ParseFloat(value[6:], bitSize)
			if err != nil {
				s = 0
			}

			aValue := h*60*60 + m*60 + s
			value = strconv.FormatFloat(aValue, 'f', 0, bitSize)
		}

		err := c.Message.UpdateValueByID(id, value)
		if err != nil {
			c.errLog.Printf("UpdateValueByID(): id=%s, value=%s, err=%s", id, value, err)
		}
	}

	// fmt.Println(c.Message)

	if c.store == nil {
		return
	}

	// передача в хранилище
	for i := 1; i < c.Message.Length; i++ {
		id, name, value := c.Message.Get(i)

		err := c.store.SaveParameter(id, name, value)
		if err != nil {
			c.errLog.Println("UpdateValueByID():", err)
		}
	}
}

// Run блокирующая функция, которая запускает периодический процесс
// добывания даных из Алкоробота.
func (c *Coutilier) Run(ctx context.Context) error {
	ticker := time.NewTicker(c.sleeptime)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
			c.GetMessage(ctx)
		}
	}
}
