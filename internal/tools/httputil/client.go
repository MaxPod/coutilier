package httputil

import (
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/hashicorp/go-cleanhttp"
	"github.com/hashicorp/go-retryablehttp"
	"gitlab.com/MaxPod/coutilier/internal/tools/env"
)

const (
	// Максимальное количество попыток повторных запросов.
	envClientRetryMax     = "HTTP_CLIENT_RETRY_MAX"
	DefaultClientRetryMax = 5

	// Минимальное время ожидания после очередной попытки.
	envClientRetryWaitMin     = "HTTP_CLIENT_RETRY_WAIT_MIN"
	DefaultClientRetryWaitMin = time.Second

	// Максимальное время ожидания после очередной попытки.
	envClientRetryWaitMax     = "HTTP_CLIENT_RETRY_WAIT_MAX"
	DefaultClientRetryWaitMax = 15 * time.Second
)

// ClientInterceptor перехватчик запросов http клиента.
type ClientInterceptor func(next http.RoundTripper) http.RoundTripper

// wrapClientInChainInterceptors оборачивает клиента перехватчиками запроса.
//
// код -> chain[n] -> ... -> chain[0] -> сетевой вызов, где n = len(chain-1).
func wrapClientInChainInterceptors(c *http.Client, chain ...ClientInterceptor) {
	for _, ci := range chain {
		c.Transport = ci(c.Transport)
	}
}

type ClientOpt func(opts *clientOpts)

func RetryParams(retryWaitMin, retryWaitMax time.Duration, retryMax int) ClientOpt {
	return func(opts *clientOpts) {
		opts.retryWaitMin = retryWaitMin
		opts.retryWaitMax = retryWaitMax
		opts.retryMax = retryMax
	}
}

func CheckRetry(fn retryablehttp.CheckRetry) ClientOpt {
	return func(opts *clientOpts) {
		opts.checkRetry = fn
	}
}

func Backoff(fn retryablehttp.Backoff) ClientOpt {
	return func(opts *clientOpts) {
		opts.backoff = fn
	}
}

// WithClientTLS устанавливает транспорт с клиентским конфигом TLS.
func WithClientTLS(conf *tls.Config) ClientOpt {
	return func(opts *clientOpts) {
		t := cleanhttp.DefaultPooledTransport()
		t.TLSClientConfig = conf

		opts.transport = t
	}
}

func ClientChainInterceptors(chain ...ClientInterceptor) ClientOpt {
	return func(opts *clientOpts) {
		opts.chainInterceptors = chain
	}
}

type clientOpts struct {
	transport    http.RoundTripper
	retryWaitMin time.Duration
	retryWaitMax time.Duration
	retryMax     int
	checkRetry   retryablehttp.CheckRetry
	backoff      retryablehttp.Backoff

	chainInterceptors []ClientInterceptor
}

func defaultClientOpts() *clientOpts {
	return &clientOpts{
		transport:    cleanhttp.DefaultPooledTransport(),
		retryWaitMin: env.Get(envClientRetryWaitMin).NeedLog().Duration(DefaultClientRetryWaitMin),
		retryWaitMax: env.Get(envClientRetryWaitMax).NeedLog().Duration(DefaultClientRetryWaitMax),
		retryMax:     env.Get(envClientRetryMax).NeedLog().Int(DefaultClientRetryMax),
		checkRetry:   retryablehttp.DefaultRetryPolicy,
		backoff:      retryablehttp.DefaultBackoff,
	}
}

func NewClient(opts ...ClientOpt) *retryablehttp.Client {
	_opts := defaultClientOpts()

	for _, opt := range opts {
		opt(_opts)
	}

	l := log.New(os.Stdout, "RTHTTPClient\t", log.Ldate|log.Ltime)

	c := &retryablehttp.Client{
		HTTPClient: &http.Client{
			Transport: _opts.transport,
		},
		RetryWaitMin: _opts.retryWaitMin,
		RetryWaitMax: _opts.retryWaitMax,
		RetryMax:     _opts.retryMax,
		CheckRetry:   _opts.checkRetry,
		Backoff:      _opts.backoff,
		Logger:       l,
	}

	wrapClientInChainInterceptors(c.HTTPClient, _opts.chainInterceptors...)

	return c
}
