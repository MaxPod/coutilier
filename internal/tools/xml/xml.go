package xml

import (
	"bytes"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"regexp"

	"golang.org/x/text/transform"
)

var ErrUnsupportedCharset = errors.New("unsupported charset")

// Encode функция выполняет сериализацию структуры src в документ xml в кодировке charset.
// Функция вернет ErrUnsupportedCharset, если переданная charset не поддерживается пакетом.
//
// Поддерживаемые кодировки можно увидеть посмотрев на переменную charsetEncodings.
func Encode(src interface{}, charset string) ([]byte, error) {
	charsetEncoding, ok := charsetEncodings[charset]
	if !ok {
		return nil, ErrUnsupportedCharset
	}

	buf := bytes.NewBuffer([]byte{})
	buf.WriteString(fmt.Sprintf("<?xml version=\"1.0\" encoding=\"%s\"?>\n", charset))

	if err := xml.NewEncoder(buf).Encode(src); err != nil {
		return nil, fmt.Errorf("failed xml encode: %w", err)
	}

	b, err := charsetEncoding.NewEncoder().Bytes(buf.Bytes())
	if err != nil {
		return nil, fmt.Errorf("failed encode bytes: %w", err)
	}

	return b, nil
}

// Decode функция выполняет десереализацию xml документа из src в структуру dest и переводит символы из заданной
// кодировки из документа (<?xml version="1.0" encoding="%s"?>) в utf-8.
//
// Поддерживаемые кодировки можно увидеть посмотрев на переменную charsetEncodings.
func Decode(src []byte, dest interface{}) error {
	dec := xml.NewDecoder(bytes.NewReader(src))
	dec.CharsetReader = charsetReader

	if err := dec.Decode(dest); err != nil {
		return fmt.Errorf("error decode xml: %w", err)
	}

	return nil
}

// TransformCharsetToUTF8 функция переводит документ из кодировки, указанной в документе, в utf-8.
//
// Если в документе будет неподдерживаемая кодировка - вернется ErrUnsupportedCharset.
//
// Поддерживаемые кодировки можно увидеть, посмотрев на переменную charsetEncodings.
func TransformCharsetToUTF8(src []byte) ([]byte, error) {
	charset := ExtractEncodingFromHeader(src)

	charsetEncoding, ok := charsetEncodings[charset]
	if !ok {
		return nil, ErrUnsupportedCharset
	}

	src, err := ioutil.ReadAll(transform.NewReader(bytes.NewReader(src), charsetEncoding.NewDecoder()))
	if err != nil {
		return nil, fmt.Errorf("failed transform bytes from %s: %w", charset, err)
	}

	return SetEncodingInHeader(src, UTF8), nil
}

var regSetEncoding = regexp.MustCompile(`<\?xml.*encoding=['"](?P<encoding>[a-zA-Z0-9-]+)['"].*\?>`)

// ExtractEncodingFromHeader извлекает название кодировки из заголовка.
func ExtractEncodingFromHeader(src []byte) string {
	sm := regSetEncoding.FindSubmatch(src)

	if sm == nil || len(sm) < 2 {
		return ""
	}

	return string(sm[1])
}

// SetEncodingInHeader выполняет замену значения поля encoding из заголовка на переданную charset.
func SetEncodingInHeader(src []byte, charset string) []byte {
	sm := regSetEncoding.FindSubmatch(src)

	if sm == nil || len(sm) < 2 {
		return src
	}

	return bytes.Replace(src, sm[1], []byte(charset), 1)
}

// DecodeAndTransformToUTF8 функция объединяет в себе использование функций Decode и TransformCharsetToUTF8.
func DecodeAndTransformToUTF8(src []byte, dst interface{}) (transformedSrc []byte, err error) {
	if err = Decode(src, dst); err != nil {
		return nil, fmt.Errorf("failed decode response: %w", err)
	}

	transformedSrc, err = TransformCharsetToUTF8(src)
	if err != nil {
		return nil, fmt.Errorf("failed transform charset: %w", err)
	}

	return transformedSrc, nil
}
