package xml

import (
	"io"

	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/charmap"
)

const (
	UTF8        = "UTF-8" // отсутствует в charsetEncodings
	Windows874  = "windows-874"
	Windows1250 = "windows-1250"
	Windows1251 = "windows-1251"
	Windows1252 = "windows-1252"
	Windows1253 = "windows-1253"
	Windows1254 = "windows-1254"
	Windows1255 = "windows-1255"
	Windows1256 = "windows-1256"
	Windows1257 = "windows-1257"
	Windows1258 = "windows-1258"
	KOI8R       = "koi8r"
	KOI8U       = "koi8u"
	Iso88591    = "iso-8859-1"
	Iso88592    = "iso-8859-2"
	Iso88593    = "iso-8859-3"
	Iso88594    = "iso-8859-4"
	Iso88595    = "iso-8859-5"
	Iso88596    = "iso-8859-6"
	Iso88597    = "iso-8859-7"
	Iso88598    = "iso-8859-8"
	Iso88599    = "iso-8859-9"
	Iso885910   = "iso-8859-10"
	Iso885913   = "iso-8859-13"
	Iso885914   = "iso-8859-14"
	Iso885915   = "iso-8859-15"
)

var charsetEncodings = map[string]encoding.Encoding{
	Windows874:  charmap.Windows874,
	Windows1250: charmap.Windows1250,
	Windows1251: charmap.Windows1251,
	Windows1252: charmap.Windows1252,
	Windows1253: charmap.Windows1253,
	Windows1254: charmap.Windows1254,
	Windows1255: charmap.Windows1255,
	Windows1256: charmap.Windows1256,
	Windows1257: charmap.Windows1257,
	Windows1258: charmap.Windows1258,
	KOI8R:       charmap.KOI8R,
	KOI8U:       charmap.KOI8U,
	Iso88591:    charmap.ISO8859_1,
	Iso88592:    charmap.ISO8859_2,
	Iso88593:    charmap.ISO8859_3,
	Iso88594:    charmap.ISO8859_4,
	Iso88595:    charmap.ISO8859_5,
	Iso88596:    charmap.ISO8859_6,
	Iso88597:    charmap.ISO8859_7,
	Iso88598:    charmap.ISO8859_8,
	Iso88599:    charmap.ISO8859_9,
	Iso885910:   charmap.ISO8859_10,
	Iso885913:   charmap.ISO8859_13,
	Iso885914:   charmap.ISO8859_14,
	Iso885915:   charmap.ISO8859_15,
}

func makeReader(charset string, input io.Reader) (io.Reader, error) {
	if charsetEncoding, ok := charsetEncodings[charset]; ok {
		return charsetEncoding.NewDecoder().Reader(input), nil
	}

	return input, nil
}

func charsetReader(charset string, input io.Reader) (io.Reader, error) {
	return makeReader(charset, input)
}
