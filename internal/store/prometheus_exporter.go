package store

import (
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// PrometheusExporter -  структура экспортера в прометеус, реализует интерфейс Store
type PrometheusExporter struct {
	G      *prometheus.GaugeVec
	Server *http.Server
}

// NewPrometheusExporter - конструктор объекта PrometheusExporter
func NewPrometheusExporter(addr string) *PrometheusExporter {
	reg := prometheus.NewRegistry()

	g := prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "alcorobot_requests",
			Help: "Status parametres from alcorobot",
		},
		[]string{"id", "name"})

	reg.MustRegister(g)

	metricMux := http.NewServeMux()
	metricMux.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))

	s := &http.Server{
		Addr:    addr,
		Handler: metricMux,
	}

	return &PrometheusExporter{
		G:      g,
		Server: s,
	}
}

const bitSize = 64 // разрядность для конвертации строки в число

// SaveParameter -  метод записи данных в экспортер
func (p *PrometheusExporter) SaveParameter(id, name, strValue string) error {
	value, err := strconv.ParseFloat(strValue, bitSize)
	if err != nil {
		value = 0
	}

	p.G.WithLabelValues(id, name).Set(value)

	return nil
}
